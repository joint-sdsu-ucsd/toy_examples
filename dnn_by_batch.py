import tensorflow as tf
import tensorflow_datasets as tfds

# Load MNIST dataset
(train_data, test_data), ds_info = tfds.load(
    'mnist',
    split=['train', 'test'],
    shuffle_files=True,
    as_supervised=True,
    with_info=True,
)

def normalize_img(image, label):
    """Normalizes images: `uint8` -> `float32`."""
    return tf.cast(image, tf.float32) / 255., label


batch_size = 64
train_data = train_data.map(normalize_img).shuffle(ds_info.splits['train'].num_examples).batch(batch_size)
test_data = test_data.map(normalize_img).batch(batch_size)


class MNISTModel(tf.Module):
    def __init__(self):
        self.w1 = tf.Variable(tf.random.normal([784, 128]))
        self.b1 = tf.Variable(tf.zeros([128]))
        self.w2 = tf.Variable(tf.random.normal([128, 10]))
        self.b2 = tf.Variable(tf.zeros([10]))

    def __call__(self, x):
        x = tf.reshape(x, [-1, 784])  # Flatten images
        x = tf.nn.relu(x @ self.w1 + self.b1)
        return x @ self.w2 + self.b2


model = MNISTModel()

# Loss and optimizer
loss_object = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
optimizer = tf.keras.optimizers.Adam()


def train_step(images, labels):
    with tf.GradientTape() as tape:
        predictions = model(images)
        loss = loss_object(labels, predictions)
    gradients = tape.gradient(loss, model.trainable_variables)
    optimizer.apply_gradients(zip(gradients, model.trainable_variables))
    return loss


num_epochs = 5
for epoch in range(num_epochs):
    epoch_loss_avg = tf.keras.metrics.Mean()
    for images, labels in train_data:
        loss = train_step(images, labels)
        epoch_loss_avg.update_state(loss)
    print(f'Epoch {epoch + 1}, Loss: {epoch_loss_avg.result():.4f}')


# Test accuracy
def test_step(images, labels):
    predictions = model(images)
    pred_labels = tf.argmax(predictions, axis=1)
    return tf.reduce_mean(tf.cast(pred_labels == labels, tf.float32))


test_accuracy = tf.keras.metrics.Mean()
for images, labels in test_data:
    accuracy = test_step(images, labels)
    test_accuracy.update_state(accuracy)

print(f'Test Accuracy: {test_accuracy.result():.4f}')
